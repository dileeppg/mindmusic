/*
 * Created on 1/28/16 2:45 PM.
 * Copyright (c) 2016 Dileep P G. All rights reserved.
 * Contact: dileeppg.nitc@gmail.com
 */

package com.psyfunlab.mindmusic;

public interface OnSpiralPathPreparedListener
{
    void onSpiralPathPrepared();
}
