/*
 * Created on 3/6/16 4:41 PM.
 * Copyright (c) 2016 Dileep P G. All rights reserved.
 * Contact: dileeppg.nitc@gmail.com
 */

package com.psyfunlab.mindmusic;

import android.graphics.Paint;
import android.graphics.PointF;

/**
 * Spot information
 */
public class Spot {

    public Disc getDisc() {
        return disc;
    }

    public boolean isSwitchedOn() {
        return switchedOn;
    }

    public void toggleOnOff() {
        this.switchedOn = !this.switchedOn;

        if (this.switchedOn)
            disc.setStyle(Paint.Style.FILL_AND_STROKE);
        else
            disc.setStyle(Paint.Style.STROKE);
    }

    private Disc disc;
    private boolean switchedOn;
    private int spiralDistance;

    public Spot(Disc disc, boolean switchedOn) {
        this.disc = disc;
        this.switchedOn = switchedOn;
    }

    public void setSpiralDistance(int spiralDistance) {
        this.spiralDistance = spiralDistance;
    }

    public int getSpiralDistance() {
        return spiralDistance;
    }
}
