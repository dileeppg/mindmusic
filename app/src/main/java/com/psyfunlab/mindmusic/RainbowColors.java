/*
 * Created on 1/28/16 2:45 PM.
 * Copyright (c) 2016 Dileep P G. All rights reserved.
 * Contact: dileeppg.nitc@gmail.com
 */

package com.psyfunlab.mindmusic;

import android.graphics.Color;

/**
 * Rainbow colors Helper class
 */
public class RainbowColors {
    public static final int VIOLET = Color.parseColor("#FE0CFF");
    public static final int INDIGO = Color.parseColor("#0A81FF");
    public static final int BLUE = Color.parseColor("#0AFFFF");
    public static final int GREEN = Color.parseColor("#21FF12");
    public static final int YELLOW = Color.parseColor("#FCFF10");
    public static final int ORANGE = Color.parseColor("#FF7A0D");
    public static final int RED = Color.parseColor("#FF0A12");

    private static final int[] VIBGYOR = new int[]
            {
                    VIOLET,
                    INDIGO,
                    BLUE,
                    GREEN,
                    YELLOW,
                    ORANGE,
                    RED
            };

    public static int getColor(int index)
    {
        return VIBGYOR[index % 7];
    }
}
