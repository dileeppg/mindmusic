/*
 * Created on 1/28/16 2:45 PM.
 * Copyright (c) 2016 Dileep P G. All rights reserved.
 * Contact: dileeppg.nitc@gmail.com
 */

package com.psyfunlab.mindmusic;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Shader;
import android.view.View;

/**
 * Disc View implementation. Used for both Dots and Spots.
 */
public class Disc extends View {

    private Paint paint;
    private float radius;
    private PointF center;
    private Paint.Style style;
    private Shader shader;
    private static float STROKE_WIDTH = 2f;

    public int getColor() {
        return color;
    }

    public float getRadius() {
        return radius;
    }

    public void setColor(int color) {
        this.color = color;

        if (this.paint == null)
            this.initPaint();

        this.paint.setColor(this.color);

        this.invalidate();
    }

    public void setShader(Shader shader) {
        this.shader = shader;

        if (this.paint == null)
            this.initPaint();

        this.paint.setShader(this.shader);

        this.invalidate();
    }

    public void setStyle(Paint.Style style) {
        this.style = style;

        if (this.paint == null)
            this.initPaint();

        this.paint.setStyle(this.style);

        this.invalidate();
    }

    private int color;

    public Disc(Context context) {
        super(context);
        this.initPaint();

        this.setColor(Color.WHITE);
    }

    private void initPaint() {
        this.paint = new Paint();
        this.paint.setStyle(Paint.Style.FILL_AND_STROKE);
        this.paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        this.paint.setDither(true);
        this.paint.setStrokeWidth(STROKE_WIDTH);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(this.center.y, this.center.y, this.radius, this.paint);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        this.radius = this.computeRadius(w, h);
        this.center = new PointF(w / 2f, h / 2f);
    }

    public static float computeRadius(int w, int h) {
        return Math.min(w, h) / 2 - STROKE_WIDTH / 2;
    }
}
