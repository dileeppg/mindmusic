/*
 * Created on 1/28/16 2:45 PM.
 * Copyright (c) 2016 Dileep P G. All rights reserved.
 * Contact: dileeppg.nitc@gmail.com
 */

package com.psyfunlab.mindmusic;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PointF;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.interaxon.libmuse.ConnectionState;
import com.interaxon.libmuse.Muse;
import com.interaxon.libmuse.MuseConnectionListener;
import com.interaxon.libmuse.MuseConnectionPacket;
import com.interaxon.libmuse.MuseDataPacketType;
import com.interaxon.libmuse.MuseManager;
import com.interaxon.libmuse.MusePreset;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends Activity {

    private static final long SIGNAL_AGGREGATION_TIME = 4000;
    private static final int NUMBER_OF_SPOTS = 25;
    private static final int[] OCTAVE_BASE_INDICES = {0, 12, 24, 36, 48};

    private GameState gameState;
    private SpottedSpiral spottedSpiral;
    private Path spiralPath;
    private PathMeasure spiralPathMeasure;
    private RelativeLayout spiralRelativeLayout;
    private Muse muse;
    private int dotRadius;
    private int spotRadius;
    private int fullSpiralLength;
    private SoundPool soundPool;
    private ArrayList<Integer> spotSounds;
    public MuseDataHandler museDataHandler;
    private MuseConnectionListener museConnectionListener;
    private Button scoreButton;
    private Toast toast;
    private GameMode gameMode;
    private Timer animationTimer;
    private ArrayList<Integer> rangeSounds;
    private Context appContext;
    private Disc alphaDot;
    private Disc betaDot;
    private Disc gammaDot;
    private Disc deltaDot;
    private Disc thetaDot;
    private Disc sageDot;
    private MusicalDotAnimationHandler alphaDotAnimationHandler;
    private MusicalDotAnimationHandler betaDotAnimationHandler;
    private MusicalDotAnimationHandler gammaDotAnimationHandler;
    private MusicalDotAnimationHandler deltaDotAnimationHandler;
    private MusicalDotAnimationHandler thetaDotAnimationHandler;
    private MellowDotAnimationHandler mellowDotAnimationHandler;
    private ArrayList<Spot> spots;
    private ConnectionState museState;
    private Spot redSpot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.gameState = GameState.NOTREADY;

        this.museState = ConnectionState.UNKNOWN;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.appContext = this.getApplicationContext();

        // To improve rendering performance
        this.clearWindowBackground();

        // Customize Toast
        this.prepareToast();

        // Get UI element handles
        this.getUIElementHandles();

        // Create Sound Pool
        this.prepareSoundPool();

        // Select default mode
        this.selectMode(GameMode.SOLO);

        // Prepare Animation objects based on the mode
        this.prepareAnimationObjects();

        // Setup Muse Data and Connection listeners
        this.setupMuseListeners();

        // Things to do after the Spiral Path is drawn
        this.spottedSpiral.setSpiralPathPreparedListener(new OnSpiralPathPreparedListener() {
            @Override
            public void onSpiralPathPrepared() {
                setSpiralDependentFields();
                placeRedSpot();
                placeSpots();
                placeDots();
                scheduleAnimations();
            }
        });

        this.gameState = GameState.READY;
    }

    private void placeRedSpot() {
        this.redSpot.setSpiralDistance(0);
        this.placeSpot(this.redSpot, (int) (2 * this.spotRadius));
    }

    private void getUIElementHandles() {
        this.spiralRelativeLayout = (RelativeLayout) this.findViewById(R.id.spiral_relative_layout);
        this.spottedSpiral = (SpottedSpiral) this.findViewById(R.id.spiral_view);
        this.scoreButton = (Button) this.findViewById(R.id.score_button);
    }

    private void clearWindowBackground() {
        this.getWindow().setBackgroundDrawable(null);
    }

    private void selectMode(GameMode gameMode) {
        this.gameMode = gameMode;

        // Create red spot to start or stop dots
        this.createRedSpot();

        switch (gameMode) {
            case SOLO:
                // Create and add musical spots for every note in an octave to the layout, keep them invisible
                this.createMusicalSpots();
                // Create and add solo mode dots to the layout, keep them invisible
                this.createSoloDots();
                // Load spot sounds for every note in the octave
                this.loadSpotSounds();
                break;

            case SAGE:
                // Create and add a single sage dot to the layout, keep it invisible
                this.sageDot = this.createDot(Color.WHITE);
                // Load range sounds for every range in the octave
                this.loadRangeSounds();
                break;
        }
    }

    private void createRedSpot() {
        this.redSpot = this.createSpot(-1);
        this.redSpot.toggleOnOff();
    }

    private void loadRangeSounds() {
        this.rangeSounds = new ArrayList<>();
        this.rangeSounds.add(this.soundPool.load(this, R.raw.ring1, 1));
        this.rangeSounds.add(this.soundPool.load(this, R.raw.ring2, 1));
        this.rangeSounds.add(this.soundPool.load(this, R.raw.ring3, 1));
        this.rangeSounds.add(this.soundPool.load(this, R.raw.ring4, 1));
        this.rangeSounds.add(this.soundPool.load(this, R.raw.ring5, 1));
        this.rangeSounds.add(this.soundPool.load(this, R.raw.ring6, 1));
        this.rangeSounds.add(this.soundPool.load(this, R.raw.ring7, 1));
        this.rangeSounds.add(this.soundPool.load(this, R.raw.ring8, 1));
    }

    private void loadSpotSounds() {
        this.spotSounds = new ArrayList<>();
        this.spotSounds.add(this.soundPool.load(this, R.raw.n1, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n2, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n3, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n4, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n5, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n6, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n7, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n8, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n9, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n10, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n11, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n12, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n13, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n14, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n15, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n16, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n17, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n18, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n19, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n20, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n21, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n22, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n23, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n24, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n25, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n26, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n27, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n28, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n29, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n30, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n31, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n32, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n33, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n34, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n35, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n36, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n37, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n38, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n39, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n40, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n41, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n42, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n43, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n44, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n45, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n46, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n47, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n48, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n49, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n50, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n51, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n52, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n53, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n54, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n55, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n56, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n57, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n58, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n59, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n60, 1));
        this.spotSounds.add(this.soundPool.load(this, R.raw.n61, 1));
    }

    private void setupMuseListeners() {

        this.museDataHandler = new MuseDataHandler();

        this.museConnectionListener = new MuseConnectionListener() {
            @Override
            public void receiveMuseConnectionPacket(MuseConnectionPacket museConnectionPacket) {
                museState = museConnectionPacket.getCurrentConnectionState();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showMuseStatus(museState);
                    }
                });
            }
        };
    }

    private void showMuseStatus(ConnectionState state) {
        if (state == ConnectionState.CONNECTED)
            showToast("Muse connected!");
        else if (state == ConnectionState.DISCONNECTED)
            showToast("Muse disconnected");
        else if (state == ConnectionState.CONNECTING)
            showToast("Connecting to Muse...");
        else
            showToast("Muse status unknown");
    }

    private void prepareSoundPool() {
        //noinspection deprecation
        this.soundPool = new SoundPool(8, AudioManager.STREAM_MUSIC, 0);

//        AudioAttributes attributes = new AudioAttributes.Builder()
//                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
//                .setFlags(AudioAttributes.FLAG_AUDIBILITY_ENFORCED)
//                .setUsage(AudioAttributes.USAGE_GAME)
//                .build();
//
//        this.soundPool = new SoundPool.Builder()
//                .setAudioAttributes(attributes)
//                .setMaxStreams(1)
//                .build();
    }

    private void setSpiralDependentFields() {
        this.spiralPath = spottedSpiral.getSpiralPath();
        this.spiralPathMeasure = new PathMeasure(spiralPath, false);
        this.fullSpiralLength = (int) spiralPathMeasure.getLength();

        this.spotRadius = (int) (spottedSpiral.getRadialGap() / 3f);
        this.dotRadius = (int) (this.spotRadius / 2.5f);
    }

    private void prepareAnimationObjects() {
        switch (this.gameMode) {
            case SOLO:
                this.prepareSoloModeDotAnimators();
                break;

            case SAGE:
                this.prepareSageModeDotAnimator();
                break;
        }
    }

    private void prepareSageModeDotAnimator() {
        this.mellowDotAnimationHandler = new MellowDotAnimationHandler(this.sageDot, MuseDataPacketType.MELLOW);
    }

    private void prepareSoloModeDotAnimators() {
        this.deltaDotAnimationHandler = new MusicalDotAnimationHandler(this.deltaDot, MuseDataPacketType.DELTA_SCORE, OCTAVE_BASE_INDICES[2]);
        this.thetaDotAnimationHandler = new MusicalDotAnimationHandler(this.thetaDot, MuseDataPacketType.THETA_SCORE, OCTAVE_BASE_INDICES[2]);
        this.alphaDotAnimationHandler = new MusicalDotAnimationHandler(this.alphaDot, MuseDataPacketType.ALPHA_SCORE, OCTAVE_BASE_INDICES[3]);
        this.betaDotAnimationHandler = new MusicalDotAnimationHandler(this.betaDot, MuseDataPacketType.BETA_SCORE, OCTAVE_BASE_INDICES[3]);
        this.gammaDotAnimationHandler = new MusicalDotAnimationHandler(this.gammaDot, MuseDataPacketType.GAMMA_SCORE, OCTAVE_BASE_INDICES[3]);
    }

    private void checkPlaySpotSound(int spotIndex, int octaveBaseIndex) {
        if (this.spots.get(spotIndex).isSwitchedOn()) {
            int soundId = spotSounds.get(octaveBaseIndex + spotIndex);
            int streamId = soundPool.play(soundId, 1, 1, 1, 0, 1);
//        Log.d("Spot Sound", "Stream Id: " + streamId);
        }
    }

    private void playRangeSound(int rangeId) {
        int soundId = rangeSounds.get(rangeId);
        int streamId = soundPool.play(soundId, 1, 1, 1, 0, 1);
//        Log.d("Range Sound", "Stream Id: " + streamId);
    }

    private void placeSpots() {
        int spotDiameter = 2 * this.spotRadius;

        switch (this.gameMode) {
            case SAGE:
                break;

            case SOLO:
                this.placeSpots(spotDiameter);
                break;
        }
    }

    private void placeSpots(int spotDiameter) {

        int spotGap = this.fullSpiralLength / (NUMBER_OF_SPOTS + 1);
        int spiralDistance = spotGap;

        for (int i = 0; i < NUMBER_OF_SPOTS; i++) {
            Spot spot = this.spots.get(i);
            spot.setSpiralDistance(spiralDistance);
            this.placeSpot(spot, spotDiameter);
            spiralDistance += spotGap;
        }
    }

    private void placeDots() {
        int dotDiameter = 2 * this.dotRadius;

        switch (this.gameMode) {
            case SAGE:
                this.placeDot(this.sageDot, dotDiameter);
                break;

            case SOLO:
                this.placeDot(this.alphaDot, dotDiameter);
                this.placeDot(this.betaDot, dotDiameter);
                this.placeDot(this.gammaDot, dotDiameter);
                this.placeDot(this.deltaDot, dotDiameter);
                this.placeDot(this.thetaDot, dotDiameter);
                break;
        }
    }

    private void placeSpot(Spot spot, int diameter) {
        Disc disc = spot.getDisc();

        this.sizeDisc(disc, diameter);

        PointF spotPosition = this.positionDisc(disc, spot.getSpiralDistance(), diameter / 2);
        disc.setColor(this.spottedSpiral.getColor(spotPosition));

        this.showDisc(disc);
    }

    private int getColor(int spotIndex) {
        return 0;
    }

    private void placeDot(Disc dot, int diameter) {
        this.sizeDisc(dot, diameter);
        this.positionDisc(dot, 0, diameter / 2);
        this.showDisc(dot);
    }

    private void showDisc(Disc disc) {
        disc.setVisibility(View.VISIBLE);
    }

    private void sizeDisc(Disc disc, int diameter) {
        disc.setLayoutParams(new RelativeLayout.LayoutParams(diameter, diameter));
    }

    private PointF positionDisc(Disc disc, int distance, int radius) {
        float[] position = new float[2];
        spiralPathMeasure.getPosTan(distance, position, new float[2]);
        disc.setTranslationX(position[0] - radius);
        disc.setTranslationY(position[1] - radius);
        return new PointF(position[0], position[1]);
    }

    private int getRange(int spiralDistance) {
        int spotCount = this.spots.size();

        for (int i = 0; i < spotCount; i++) {
            int spotDistance = this.spots.get(i).getSpiralDistance();
            if (spiralDistance <= spotDistance)
                return i;
        }

        return spotCount;
    }

    private void scheduleAnimations() {
        this.animationTimer = new Timer();

        switch (this.gameMode) {
            case SOLO:
                this.animationTimer.scheduleAtFixedRate(this.alphaDotAnimationHandler, 0, SIGNAL_AGGREGATION_TIME);
                this.animationTimer.scheduleAtFixedRate(this.betaDotAnimationHandler, 0, SIGNAL_AGGREGATION_TIME);
                this.animationTimer.scheduleAtFixedRate(this.gammaDotAnimationHandler, 0, SIGNAL_AGGREGATION_TIME);
                this.animationTimer.scheduleAtFixedRate(this.deltaDotAnimationHandler, 0, SIGNAL_AGGREGATION_TIME);
                this.animationTimer.scheduleAtFixedRate(this.thetaDotAnimationHandler, 0, SIGNAL_AGGREGATION_TIME);

                break;

            case SAGE:
                this.animationTimer.scheduleAtFixedRate(this.mellowDotAnimationHandler, 0, SIGNAL_AGGREGATION_TIME);
                break;
        }
    }

    private void checkConnectMuse() {
        try {
            MuseManager.refreshPairedMuses();
            List<Muse> pairedMuses = MuseManager.getPairedMuses();

            if (pairedMuses.size() < 1) {
                this.showToast("Muse unreachable!");
                return;
            }

            this.muse = pairedMuses.get(0);
            ConnectionState state = muse.getConnectionState();

            if (state == ConnectionState.CONNECTED || state == ConnectionState.CONNECTING) {
                this.showMuseStatus(state);
                return;
            }

            this.showToast("Initiating connection to Muse...");

            this.registerMuseListeners();
            this.muse.setPreset(MusePreset.PRESET_14);
            this.muse.enableDataTransmission(true);
            this.muse.runAsynchronously();
        } catch (Exception e) {
            showToast("Error connecting to Muse");
            Log.e("Connection Error", e.toString());
        }
    }

    private void registerMuseListeners() {
        this.muse.registerConnectionListener(this.museConnectionListener);
        this.muse.registerDataListener(this.museDataHandler, MuseDataPacketType.MELLOW);
        this.muse.registerDataListener(this.museDataHandler, MuseDataPacketType.ALPHA_SCORE);
        this.muse.registerDataListener(this.museDataHandler, MuseDataPacketType.BETA_SCORE);
        this.muse.registerDataListener(this.museDataHandler, MuseDataPacketType.GAMMA_SCORE);
        this.muse.registerDataListener(this.museDataHandler, MuseDataPacketType.DELTA_SCORE);
        this.muse.registerDataListener(this.museDataHandler, MuseDataPacketType.THETA_SCORE);
    }

    private void prepareToast() {
        this.toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
        this.toast.setGravity(Gravity.TOP, 0, (int) this.getResources().getDimension(R.dimen.toast_y_offset));
    }

    private void showToast(String message) {
        this.toast.setText(message);
        this.toast.show();
    }

    private void createMusicalSpots() {
        this.spots = new ArrayList<Spot>();

        for (int i = 0; i < NUMBER_OF_SPOTS; i++)
            this.spots.add(this.createSpot(i));
    }

    private Spot createSpot(int spotNumber) {
        Disc disc = new Disc(this.appContext);
        disc.setLayoutParams(new ViewGroup.LayoutParams(0, 0));
        disc.setStyle(Paint.Style.STROKE);
        this.spiralRelativeLayout.addView(disc);

        final Spot spot = new Spot(disc, false);
        disc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spot.toggleOnOff();
            }
        });

        return spot;
    }

    private void createSoloDots() {
        this.gammaDot = this.createDot(Color.parseColor("#BBFE0CFF")); // Violet

        this.betaDot = this.createDot(Color.parseColor("#BB0A81FF")); // Indigo

        this.alphaDot = this.createDot(Color.parseColor("#DD21FF12")); // Green

        this.thetaDot = this.createDot(Color.parseColor("#DDFCFF10")); // Yellow

        this.deltaDot = this.createDot(Color.parseColor("#BBFF7A0D")); // Orange
    }

    private Disc createDot(int color) {
        Disc dot = new Disc(this.appContext);
        dot.setLayoutParams(new ViewGroup.LayoutParams(0, 0));
        dot.setColor(color);
        this.spiralRelativeLayout.addView(dot);
        return dot;
    }

    public void musePowerButtonOnClick(View view) {
        this.checkConnectMuse();
    }

    public void scoreButtonOnClick(View view) {
        showToast("Calmness Score");
    }

    private static boolean isBetween(int x, int a, int b) {
        return ((x - a) * (x - b) <= 0);
    }

    private class MellowDotAnimationHandler extends TimerTask implements ValueAnimator.AnimatorUpdateListener {
        private int currentDistance;
        private int targetDistance;
        private Disc dot;
        private ValueAnimator animator;
        private MuseDataPacketType dataType;

        public MellowDotAnimationHandler(Disc dot, MuseDataPacketType dataType) {
            this.dot = dot;
            this.dataType = dataType;

            this.currentDistance = 0;
            this.targetDistance = 0;

            this.animator = ValueAnimator.ofInt();
            this.animator.addUpdateListener(this);
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            currentDistance = (int) animation.getAnimatedValue();
            positionDisc(this.dot, this.currentDistance);
        }

        @Override
        public void run() {
            Double currentScore = museDataHandler.getCurrentValue(this.dataType);
            targetDistance = (int) (currentScore * fullSpiralLength);
            int rangeId = getRange(targetDistance);
            playRangeSound(rangeId);

            Double averageScore = museDataHandler.getAverageValue(this.dataType);
            final int percentageScore = (int) Math.round(100 * averageScore);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    scoreButton.setText(percentageScore + "%");
                    animateDot();
                }
            });
        }

        private void animateDot() {
            this.animator.setIntValues(this.currentDistance, this.targetDistance);
            this.animator.setDuration(SIGNAL_AGGREGATION_TIME);
            this.animator.start();
        }
    }

    private void positionDisc(Disc disc, int currentDistance) {
        this.positionDisc(disc, currentDistance, (int) disc.getRadius());
    }

    private class MusicalDotAnimationHandler extends TimerTask implements ValueAnimator.AnimatorUpdateListener {

        private MuseDataPacketType dataType;
        private int currentDistance;
        private int targetDistance;
        private Disc dot;
        private ValueAnimator animator;
        private int direction;
        private ArrayList<Integer> targetSpotIndices;
        private int octaveBaseIndex;

        Random random = new Random();

        public MusicalDotAnimationHandler(Disc dot, MuseDataPacketType dataType, int octaveBaseIndex) {
            targetDistance = 0;
            currentDistance = 0;
            targetSpotIndices = new ArrayList<>();

            this.animator = ValueAnimator.ofInt();
            this.animator.addUpdateListener(this);

            this.dot = dot;
            this.dataType = dataType;
            this.octaveBaseIndex = octaveBaseIndex;
        }

        @Override
        public void run() {

            refreshCalmnessScore();

            if (redSpot.isSwitchedOn()) {
                targetDistance = 0;
            }
            else {
                if (museState == ConnectionState.CONNECTED) {
                    Double newScore = museDataHandler.getCurrentValue(dataType);
                    targetDistance = (int) (newScore * fullSpiralLength);
                }
                else {
                    targetDistance = (int) (random.nextFloat() * fullSpiralLength);
                }
            }

            if (targetDistance >= currentDistance)
                direction = 1;
            else
                direction = -1;

            targetSpotIndices.clear();
            for (int spotIndex = 0; spotIndex < spots.size(); spotIndex++) {
                int distance = spots.get(spotIndex).getSpiralDistance();
                if (isBetween(distance, currentDistance, targetDistance))
                    targetSpotIndices.add(spotIndex);
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    animateDot();
                }
            });
        }

        private void animateDot() {
            this.animator.setIntValues(this.currentDistance, this.targetDistance);
            this.animator.setDuration(SIGNAL_AGGREGATION_TIME);
            this.animator.start();
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            currentDistance = (int) animation.getAnimatedValue();

            int spotsRemaining = targetSpotIndices.size();
            if (spotsRemaining > 0) {
                int nextTargetSpotIndex;

                if (direction == 1) {
                    nextTargetSpotIndex = targetSpotIndices.get(0);
                    if (currentDistance >= spots.get(nextTargetSpotIndex).getSpiralDistance()) {
                        checkPlaySpotSound(nextTargetSpotIndex, this.octaveBaseIndex);
                        targetSpotIndices.remove(0);
                    }
                } else {
                    nextTargetSpotIndex = targetSpotIndices.get(spotsRemaining - 1);
                    if (currentDistance <= spots.get(nextTargetSpotIndex).getSpiralDistance()) {
                        checkPlaySpotSound(nextTargetSpotIndex, this.octaveBaseIndex);
                        targetSpotIndices.remove(spotsRemaining - 1);
                    }
                }
            }

            positionDisc(this.dot, this.currentDistance);
        }
    }

    private void refreshCalmnessScore() {
        Double averageMellowScore = museDataHandler.getAverageValue(MuseDataPacketType.MELLOW);
        final int percentageMellowScore = (int) Math.round(100 * averageMellowScore);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                scoreButton.setText(percentageMellowScore + "%");
            }
        });
    }
}