/*
 * Created on 1/28/16 2:45 PM.
 * Copyright (c) 2016 Dileep P G. All rights reserved.
 * Contact: dileeppg.nitc@gmail.com
 */

package com.psyfunlab.mindmusic;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PointF;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;

import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * Custom View for the Spiral
 */
public class SpottedSpiral extends View {
    private Paint spotPaint;
    private ArrayList<PointF> spotPoints;

    private int maxSpiralRadius;
    private Paint dotPaint;
    private int maxSpiralAngleDegrees = 1890; // 5.25 coils of the Archimedes' spiral
    private float spotRadius;
    private ArrayList<Integer> spotAngles;
    private float spiralConstantA;
    private ArrayList<Integer> spotSpiralDistances;

    private static int[] getRainbowColors() {
        return new int[]
                {
                        RainbowColors.RED
                        , RainbowColors.ORANGE
                        , RainbowColors.YELLOW
                        , RainbowColors.GREEN
                        , RainbowColors.BLUE
                        , RainbowColors.INDIGO
                        , RainbowColors.VIOLET
//                        , RainbowColors.VIOLET
//                        , RainbowColors.INDIGO
//                        , RainbowColors.BLUE
//                        , RainbowColors.GREEN
//                        , RainbowColors.YELLOW
//                        , RainbowColors.ORANGE
//                        , RainbowColors.RED
                };
    }

    private static float[] getGradientStops() {
        return null;
    }

    private boolean useRainbowColors = false;
    public boolean isUsingRainbowColors() {
        return useRainbowColors;
    }
    public void setUseRainbowColors(boolean useRainbowColors) {
        this.useRainbowColors = useRainbowColors;
    }

    private void setShaders() {
        if (this.useRainbowColors)
            spiralPaint.setShader(this.getRainbowShader());
        else
            spiralPaint.setColor(this.strokeColor);

        spotPaint.setShader(this.getRainbowShader());
    }

    private float spiralCenterX;
    private float spiralCenterY;

    private OnSpiralPathPreparedListener spiralPathPreparedListener;
    public void setSpiralPathPreparedListener(OnSpiralPathPreparedListener listener)
    {
        this.spiralPathPreparedListener = listener;
    }

    private int strokeColor = Color.GREEN; // default color
    public int getStrokeColor() {
        return strokeColor;
    }
    public void setStrokeColor(int strokeColor) {
        this.strokeColor = strokeColor;
    }

    float strokeWidth;
    public float getStrokeWidth() {
        return strokeWidth;
    }
    public void setStrokeWidth(float strokeWidth) {
        this.strokeWidth = strokeWidth;
        this.spiralPaint.setStrokeWidth(strokeWidth);
    }

    private Path spiralPath;
    private Paint spiralPaint;

    public SpottedSpiral(Context context) {
        super(context);
        init(null, 0);
    }

    public SpottedSpiral(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public SpottedSpiral(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {

        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.SpottedSpiral, defStyle, 0);

        this.strokeColor = a.getColor(R.styleable.SpottedSpiral_strokeColor, this.strokeColor);

        this.strokeWidth = getResources().getDimension(R.dimen.spiral_stroke_width);

        this.useRainbowColors = a.getBoolean(R.styleable.SpottedSpiral_useRainbowColors, this.useRainbowColors);

        a.recycle();

        this.prepareSpiralPaint();
        this.prepareSpotPaint();
        this.prepareDotPaint();

        this.setShaders();
    }

    private void prepareSpiralPaint() {
        this.spiralPaint = new Paint();
        spiralPaint.setStyle(Paint.Style.STROKE);
        spiralPaint.setStrokeWidth(this.strokeWidth);
        spiralPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        spiralPaint.setDither(true);
    }

    private void prepareSpotPaint() {
        this.spotPaint = new Paint();
        spotPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        spotPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        // spotPaint.setStrokeWidth(2f);
        // spotPaint.setPathEffect(new DashPathEffect(new float[] {5f, 3f}, 0f));
        spotPaint.setDither(true);
    }

    private void prepareDotPaint() {
        this.dotPaint = new Paint();
        dotPaint.setStyle(Paint.Style.FILL);
        dotPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        dotPaint.setDither(true);
        dotPaint.setColor(Color.WHITE);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        this.setShaders();

        canvas.drawPath(spiralPath, spiralPaint);

        this.plotSpots(canvas);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
    }

    @Override
    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);

        this.spiralCenterX = width / 2f;
        this.spiralCenterY = height / 2f;

        this.prepareSpiralPath(width, height);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }

    private void prepareSpiralPath(int spiralWidth, int spiralHeight) {

        // Compute radius of the Spiral based on available space
        int maxPadding = this.getMaximumPadding();
        int maxSpiralDiameter = min(spiralWidth, spiralHeight) - (int)this.strokeWidth - 2 * maxPadding;
        this.maxSpiralRadius = maxSpiralDiameter / 2;

        // Compute constant a in the polar equation for the Archimedes' Spiral r = a * theta
        this.spiralConstantA = (float) this.maxSpiralRadius / (float) this.maxSpiralAngleDegrees;

        // Compute Spot Radius ensuring spots don't overlap even on small screens
        this.spotRadius = spiralConstantA * 360 / 4f;
        this.spotPoints = new ArrayList<>();
        this.spotAngles = new ArrayList<>();
        this.spotSpiralDistances = new ArrayList<>();

        this.spiralPath = new Path();
        this.spiralPath.moveTo(spiralCenterX, spiralCenterY);

        float x, y, r;
        double thetaRadians;

        int fretAngle = this.maxSpiralAngleDegrees / 13;

        for (int thetaDegrees = 0; thetaDegrees <= this.maxSpiralAngleDegrees; thetaDegrees++) {
            thetaRadians = Math.toRadians(thetaDegrees);
            r = spiralConstantA * thetaDegrees; // Radius of the Archimedes' Spiral
            x = (float) (r * Math.cos(thetaRadians) + spiralCenterX);
            y = (float) (r * Math.sin(thetaRadians) + spiralCenterY);
            this.spiralPath.lineTo(x, y);

//            if (thetaDegrees > 0 && thetaDegrees % fretAngle == 0) {
//                this.spotPoints.add(new PointF(x, y));
//                this.spotAngles.add(thetaDegrees);
//                int spotDistance = (int) new PathMeasure(this.spiralPath, false).getLength();
//                this.spotSpiralDistances.add(spotDistance);
//            }
        }

        post(new Runnable() {
            @Override
            public void run() {
                if (spiralPathPreparedListener != null)
                    spiralPathPreparedListener.onSpiralPathPrepared();
            }
        });
    }

    private void plotSpots(Canvas canvas) {

        if (this.spotPoints != null) {
            for (int i = 0; i < this.spotPoints.size(); i++) {
                PointF spot = this.spotPoints.get(i);
                canvas.drawCircle(spot.x, spot.y, this.spotRadius, this.spotPaint);
            }
        }

    }

    /**
     * Gets the ringPath
     *
     * @return The string Path object.
     */
    public Path getSpiralPath() {
    return this.spiralPath;
    }

    public int getMaximumPadding() {

        int maxPadding = max(getPaddingLeft(),
                max(getPaddingRight(),
                        max(getPaddingTop(),
                                getPaddingBottom()
                        )
                )
        );

        return maxPadding;
    }

    public Shader getRainbowShader() {

        RadialGradient rainbowGradient = new RadialGradient(
                spiralCenterX,
                spiralCenterY,
                1 + maxSpiralRadius,
                getRainbowColors(),
                getGradientStops(),
                Shader.TileMode.CLAMP
        );

        return rainbowGradient;
    }

    public float getSpotRadius() {
        return spotRadius;
    }

    public ArrayList<Integer> getSpotAngles() {
        return spotAngles;
    }

    public ArrayList<Integer> getSpotSpiralDistances() {
        return this.spotSpiralDistances;
    }

    public float getRadialGap() {
        return this.spiralConstantA * 360;
    }

    public int getColor(PointF position) {
        this.setDrawingCacheEnabled(true);
        return this.getDrawingCache().getPixel((int) position.x, (int) position.y);
    }
}
