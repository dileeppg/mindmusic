/*
 * Created on 2/4/16 11:46 AM.
 * Copyright (c) 2016 Dileep P G. All rights reserved.
 * Contact: dileeppg.nitc@gmail.com
 */

package com.psyfunlab.mindmusic;

public enum GameMode {
    SOLO,
    SAGE,
    DUET,
    DUEL
}
