/*
 * Created on 1/28/16 2:45 PM.
 * Copyright (c) 2016 Dileep P G. All rights reserved.
 * Contact: dileeppg.nitc@gmail.com
 */

package com.psyfunlab.mindmusic;

/**
 * Game States
 */
public enum GameState {
    NOTREADY,
    READY,
    STARTING,
    RUNNING,
    ENDING,
    OVER
}
