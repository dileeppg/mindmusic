/*
 * Created on 2/4/16 11:29 AM.
 * Copyright (c) 2016 Dileep P G. All rights reserved.
 * Contact: dileeppg.nitc@gmail.com
 */

package com.psyfunlab.mindmusic;

import android.util.Log;

import com.interaxon.libmuse.Eeg;
import com.interaxon.libmuse.MuseArtifactPacket;
import com.interaxon.libmuse.MuseDataListener;
import com.interaxon.libmuse.MuseDataPacket;
import com.interaxon.libmuse.MuseDataPacketType;

import java.util.ArrayList;
import java.util.HashMap;

public class MuseDataHandler extends MuseDataListener {
    private HashMap<MuseDataPacketType, MuseData> museDataMap;

    public MuseDataHandler() {
        this.museDataMap = new HashMap();
    }

    public Double getCurrentValue(MuseDataPacketType dataType) {
        MuseData museData = this.museDataMap.get(dataType);
        if (museData == null)
            return 0d;

        return museData.CurrentValue;
    }

    public Double getAverageValue(MuseDataPacketType dataType) {
        MuseData museData = this.museDataMap.get(dataType);
        if (museData == null)
            return 0d;

        return museData.AverageValue;
    }

    public void resetValues(MuseDataPacketType dataType) {
        MuseData museData = this.museDataMap.get(dataType);
        if (museData != null) {
            museData.CurrentValue = 0d;
            museData.AverageValue = 0d;
            museData.AggregateValue = 0d;
            museData.ValueCount = 0;
        }
    }

    @Override
    public void receiveMuseDataPacket(MuseDataPacket museDataPacket) {
        ArrayList<Double> data = museDataPacket.getValues();
        MuseDataPacketType dataType = museDataPacket.getPacketType();
        Double value;

        switch (dataType) {
            case ALPHA_RELATIVE:
            case BETA_RELATIVE:
            case GAMMA_RELATIVE:
            case DELTA_RELATIVE:
            case THETA_RELATIVE:
            case ALPHA_SCORE:
            case BETA_SCORE:
            case GAMMA_SCORE:
            case DELTA_SCORE:
            case THETA_SCORE:
                Double value1 = data.get(Eeg.TP9.ordinal());
                Double value2 = data.get(Eeg.FP1.ordinal());
                Double value3 = data.get(Eeg.FP2.ordinal());
                Double value4 = data.get(Eeg.TP10.ordinal());

                value = (value1 + value2 + value3 + value4) / 4;

                this.updateMuseData(value, dataType);

                break;

            case MELLOW:
            case CONCENTRATION:
                value = data.get(0);
                this.updateMuseData(value, dataType);
                break;
        }
    }

    private void updateMuseData(Double value, MuseDataPacketType dataType) {

        MuseData museData = this.museDataMap.get(dataType);

        if (museData == null) {
            museData = new MuseData(dataType);
            this.museDataMap.put(dataType, museData);
        }

        museData.CurrentValue = value;
        if (!Double.isNaN(value) && value > 0d) {
            museData.ValueCount++;
            museData.AggregateValue += value;
            museData.AverageValue = museData.AggregateValue / museData.ValueCount;
        }

        Log.d("Type", String.valueOf(dataType));
        Log.d("Current", String.valueOf(museData.CurrentValue));
        Log.d("Count", String.valueOf(museData.ValueCount));
        Log.d("Aggregate", String.valueOf(museData.AggregateValue));
        Log.d("Average", String.valueOf(museData.AverageValue));
    }

    @Override
    public void receiveMuseArtifactPacket(MuseArtifactPacket museArtifactPacket) {

    }

    private class MuseData {
        public int ValueCount;
        public Double AggregateValue;
        public Double CurrentValue;
        public Double AverageValue;
        public MuseDataPacketType dataType;

        public MuseData(MuseDataPacketType dataType) {
            this.dataType = dataType;
            CurrentValue = 0d;
            ValueCount = 0;
            AggregateValue = 0d;
            AverageValue = 0d;
        }
    }
}
