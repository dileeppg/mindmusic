MIND MUSIC - A Brain controlled Music game (India Hacks Phase 2)
================================================================

Note: This game works best with the Muse headband from InteraXon

1. Go to https://play.google.com/apps/testing/com.psyfunlab.mindmusic, click on 'BECOME A TESTER' and install the MindMusic App on your Android phone (Alternatively, you can build this source code, transfer the apk file onto your Android phone and install it).

2. Get the MUSE headset (www.choosemuse.com).

3. Follow the device instructions to pair the headband with your Android phone.

4. Launch the MindMusic app.

5. Ensure the device is switched on.

6. Hit the gray power button to connect to the MUSE (It should connect in a few seconds).

7. Slide the headband on your head (see device instructions).

8. Wait for a minute to allow stable EEG signal.

9. Hit the center red spot to unleash the dots. The movement is based on real time brain wave parameters.

10. Select one or more of the other spots along the spiral. The tune is based on this selection. Experiment to find out what is pleasing to your ears.

11. Close your eyes and meditate on your inner music. 

12. Use the calmness score to figure out which tune is really making you calmer.

13. Hit the center red spot again to pull the dots back and pause the music.

14. Keep calm and enjoy your own music!